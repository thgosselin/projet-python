import random 

def importer_mots(nom_fichier):
    L=[]
    with open (nom_fichier) as f:
        for i in f:
            if len(i)>=3:  # on exclut les mots trop courts
                texte=i.strip()
                texte_minuscule=texte.lower()  # on met tous les mots en minuscule
                L.append(texte_minuscule)
    return L

def choisir_mot_alea(liste_mots):
    nombre=random.randint(0,len(liste_mots)-1) # on choisit un nombre aléatoire entre 0 et la longueur de la liste de mots
    mot_choisi = liste_mots[nombre]  # on récupère le mot correspondant à ce nombre aléatoire
    return(mot_choisi)

def initialiser_mot_part_decouv(mot_myst,car_subst="-"):
    l=[]
    s=list(mot_myst)
    l.append(s[0])
    for i in range(1,len(s)-1):
        l.insert(i,car_subst) # pour chaque lettre dans le mot, on l'ajoute à une liste en utilisant le caractère de substitution
    return l
    
def afficher_potence_texte(nb_err,nb_err_max):
    message = "PERDU" + "!" * (nb_err_max - 5)
    message = message[:nb_err] + "–" * (nb_err_max - nb_err) + "!" # le nombre d'erreurs est représenté par le nombre de tirets de la potence
    print(message)

def demander_proposition(deja_dit):
    mot_maj=[]
    for mot in deja_dit:
        mot_maj.append(mot.upper())
    while True:
        proposition = input("Saisissez une lettre: ").upper()
        if len(proposition) == 1 and proposition.isalpha() and proposition not in mot_maj and proposition not in deja_dit: #isalpha() sert à savoir si la proposition est bien une lettre
            return proposition  # le joueur doit saisir une lettre qui n'a pas encore été proposée
        else:
            print("Saisie incorrecte, veuillez saisir une lettre qui n'a pas encore été proposée.")    
            
def decouvrir_lettre(lettre, mot_myst, lmot_decouv):
    nouvelle_lettre_decouverte = False
    for i in range(len(mot_myst)):
        if mot_myst[i] == lettre:
            if lmot_decouv[i] == "-":
            	lmot_decouv[i] = lettre
            	nouvelle_lettre_decouverte = True
            print(lmot_decouv) # Affiche le mot partiellement découvert après chaque découverte de lettre
    return nouvelle_lettre_decouverte


def partie_humain(mot_myst, nb_err_max, car_subst="-"):
    lmot_decouv = initialiser_mot_part_decouv(mot_myst, car_subst)
    deja_dit = [] # initialise une liste pour stocker les lettres déjà proposées par le joueur
    nb_err = 0 # initialise le nombre d'erreurs à 0
    gagne = False # initialise le booléen de victoire à False
    while nb_err < nb_err_max and not gagne: # boucle tant que le joueur n'a pas atteint le nombre maximal d'erreurs autorisé ou qu'il n'a pas trouvé toutes les lettres du mot mystère
        mot_affiche = "".join(lmot_decouv).upper()  # affiche le mot découvert en majuscules
        print("Mot à deviner : ", mot_affiche)
        if deja_dit: # si le joueur a déjà proposé des lettres
            deja_dit_affiche = ", ".join(deja_dit).upper() # affiche les lettres déjà proposées en majuscules et séparées par des virgules
            print("Lettres déjà proposées : ", deja_dit_affiche) # demande au joueur de proposer une lettre qui n'a pas encore été proposée
        proposition = demander_proposition(deja_dit) # découvre la lettre proposée dans le mot mystère s'il y en a
        deja_dit.append(proposition)
        lettre_presente = decouvrir_lettre(proposition, mot_myst, lmot_decouv)
        if lettre_presente:
            print("Lettre présente !")
        else:
            nb_err += 1 # incrémente le nombre d'erreurs s'il n'y a pas de lettre trouvée
            print("Erreur n°", nb_err)
            if nb_err > 1: # s'il y a plus d'une erreur, affiche la potence du pendu
                afficher_potence_texte(nb_err, nb_err_max)
        if "-" not in lmot_decouv: # s'il n'y a plus de caractère de substitution dans la liste des lettres découvertes, le joueur a gagné
            gagne = True
    print()
    if gagne:
        print("Félicitations, vous avez gagné !")
        print("Le mot à deviner était : ", mot_myst.upper())
        return True
    else:
        print("Dommage, vous avez perdu.")
        print("Le mot à deviner était : ", mot_myst.upper())
        return False    
 
def partie_humain_alea(nom_fichier, nb_err_max, car_subst="-"):
    liste_mots = importer_mots(nom_fichier) # importer la liste de mots depuis le fichier texte
    mot_myst = choisir_mot_alea(liste_mots) # choisir un mot aléatoire depuis la liste
    return partie_humain(mot_myst, nb_err_max, car_subst) # jouer la partie manuelle avec le mot choisi aléatoirement
    
def fabrique_liste_alphabet():
    liste_propose=[]
    for i in range (65,90):
        liste_propose.append(chr(i))
    return liste_propose

def fabrique_liste_alea():
    liste_alphabet = fabrique_liste_alphabet() # créer la liste de toutes les lettres de l'alphabet en majuscules
    random.shuffle(liste_alphabet) # mélanger la liste de manière aléatoire
    return liste_alphabet

def dico_frequence(nom_fichier):
    dic = {}
    with open(nom_fichier, "r") as fichier: # ouvrir le fichier en mode lecture
        for ligne in fichier: # parcourir chaque ligne du fichier
            for lettre in ligne.strip(): # parcourir chaque lettre de la ligne, sans les caractères de fin de ligne
                if lettre.isalpha() and lettre.isupper():#on test si la lettre une alphabet et une majuscule
                    if lettre in dic: # si la lettre est déjà dans le dictionnaire, ajouter 1 à sa fréquence
                        dic[lettre] += 1
                    else:
                        dic[lettre] = 1
    return dic


def partie_auto(mot_myst, liste_lettres, affichage=True, pause=False):
    mot_decouvert = "_" * len(mot_myst)
    erreurs = 0
    
    while "_" in mot_decouvert:
        lettre_proposee = ""
        for lettre in liste_lettres:
            if lettre not in mot_decouvert:
                lettre_proposee = lettre
                break
        lettre_trouvee = False
        for i in range(len(mot_myst)):
            if mot_myst[i] == lettre_proposee and mot_decouvert[i] == "_":
                mot_decouvert = mot_decouvert[:i] + lettre_proposee + mot_decouvert[i+1:]
                lettre_trouvee = True
        if not lettre_trouvee:
            erreurs += 1
        if affichage:
            print(mot_decouvert)
        if pause:
            input("Appuyez sur Entrée pour continuer...")
    if affichage:
        if erreurs == 0:
            print("Gagné !")
        else:
            print("Perdu... Le mot était :", mot_myst)
            
print("Bienvenue au jeu du Pendu !")
print("Choisissez une option :")
print("1 - Partie manuelle par un humain")
print("2 - Partie automatique par l'ordinateur avec choix de la stratégie d'apprentissage des fréquences de lettres")
print("3 - Sortie menu")
choix=input("Entrz le numéro de l'option choisie : ")
if choix == "1":
	partie_humain_alea("mots.txt",8)
elif choix == "2" :
	mot_myst = input("Entrez un mot mystère : ")
	liste_lettres = input("Entrez la liste de lettre : ")
	partie_auto(mot_myst,liste_lettres,affichage=True, pause=False)
elif choix == "3" :
	print("Sortie de fonction menu")
else:
	print("Option invalide, veuillez entrer 1 ou 2 ou 3.") 
